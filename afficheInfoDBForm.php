<?php
include_once ('db_connect.php');
/** @var PDO $db */
?>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
</head>
<body style="padding: 30px">

<h1>Connexion</h1>
<form name="form" method="post">
    <p><label for="password">Mot de passe</label> <input type="password" title="Saisissez le mot de passe" name="password" /></p>
    <p><input type="submit" name="submit" value="Connexion" /></p>
</form>

<?php
if (isset($_POST['submit'])) {
	if (isset($_POST['password']) AND $_POST['password'] ==  "roxanacontactsolaneos") // Si le mot de passe est bon
	{
		?>
        <div class="container" style="width: 100%">
            <form method="post" action="export.php">
                <input type="submit" name="export" value="Sortir en CSV" class="btn btn-danger">
            </form>

            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr class="btn-primary">
                    <td>id</td>
                    <td>nom</td>
                    <td>prénom</td>
                    <td>email</td>
                    <td>telephone</td>
                    <td>message</td>
                </tr>
                </thead>
                <tbody>
				<?php
				$sql = "select * from info ORDER BY id asc";
				$query = $db->query($sql);
				$i = 1;
				while ($row = $query->fetch()) // fetch all data from the database
				{
					?>
                    <tr>
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['nom']; ?></td>
                        <td><?php echo $row['prenom']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['tel']; ?></td>
                        <td><?php echo $row['messag']; ?></td>
                    </tr>

					<?php
					$i++;
				}
				?>
                </tbody>
            </table>
        </div>
		<?php
	} else {
		echo '<p>Mot de passe incorrect</p>';
	}
}
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
</body>
</html>