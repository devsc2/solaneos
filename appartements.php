<?php  

session_start();


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="viewport" content="Solaneos met à votre disposition des appartements dans un cadre de vie agréable et calme. N'hésitez pas à les découvrir !">
    <title>Solaneos - Appartement</title>
    <link rel="icon" href="./assets/img/favicon.png" />
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/Fixed-navbar-starting-with-transparency-1.css">
    <link rel="stylesheet" href="assets/css/Fixed-navbar-starting-with-transparency.css">
    <link rel="stylesheet" href="assets/css/Bold-BS4-Footer-Big-Logo.css">
    <link rel="stylesheet" href="assets/css/Bold-BS4-Full-Page-Image-Slider-Header.css">
    <link rel="stylesheet" href="assets/css/card-3-column-animation-shadows-images.css">
    <link rel="stylesheet" href="assets/css/Carousel_Image_Slider-1.css">
    <link rel="stylesheet" href="assets/css/Carousel_Image_Slider.css">
    <link rel="stylesheet" href="assets/css/Customizable-Background--Overlay.css">
    <link rel="stylesheet" href="assets/css/Dark-NavBar-1.css">
    <link rel="stylesheet" href="assets/css/Dark-NavBar-2.css">
    <link rel="stylesheet" href="assets/css/Dark-NavBar.css">
    <link rel="stylesheet" href="assets/css/gradient-navbar-1.css">
    <link rel="stylesheet" href="assets/css/gradient-navbar.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Map-Clean.css">
    <link rel="stylesheet" href="assets/css/MUSA_carousel-product-cart-slider-1.css">
    <link rel="stylesheet" href="assets/css/MUSA_carousel-product-cart-slider.css">
    <link rel="stylesheet" href="assets/css/Navbar---Apple-1.css">
    <link rel="stylesheet" href="assets/css/Navbar---Apple.css">
    <link rel="stylesheet" href="assets/css/responsive-blog-card-slider.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Slider_Carousel_webalgustocom-1.css">
    <link rel="stylesheet" href="assets/css/Slider_Carousel_webalgustocom-2.css">
    <link rel="stylesheet" href="assets/css/Slider_Carousel_webalgustocom.css">
    <link rel="stylesheet" href="assets/css/slideshow_w3css-mloureiro1973-1.css">
    <link rel="stylesheet" href="assets/css/slideshow_w3css-mloureiro1973.css">
    <link rel="stylesheet" href="assets/css/Slideshow-ML-de-3-Slides.css">
    <link rel="stylesheet" href="assets/css/Swiper-Slider-1.css">
    <link rel="stylesheet" href="assets/css/Swiper-Slider-Card-For-Blog-Or-Product-1.css">
    <link rel="stylesheet" href="assets/css/Swiper-Slider-Card-For-Blog-Or-Product.css">
    <link rel="stylesheet" href="assets/css/Swiper-Slider.css">
    <link rel="stylesheet" href="assets/css/Testimonials.css">
    <link rel="stylesheet" href="assets/css/Tricky-Grid---2-Column-on-Desktop--Tablet-Flip-Order-of-12-Column-rows-on-Mobile.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
</head>

<body style="overflow-x: hidden; ">

    <div class="contactheader">
        <div style="text-align: right;padding: 10px 10% 10px 0px;background: #e4eff5;color: #304a67;font-size: 16px;font-family: 'Josefin Sans', sans-serif;"><i class="fa fa-phone" style="padding: 0px 10px 0px 50px;"></i><span><strong>06 32 82 69 59 | 06 03 01 11 17</strong></span><i class="fa fa-envelope" style="padding: 0px 10px 0px 50px;"></i><span style="font-size: 16px;"><strong>solaneoslocations@gmail.com</strong></span></div>
        <hr style="margin: -1px;border-width: 5px;border-color: rgb(239,84,108);">
</div>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top navbar-transparency" style="position: relative;background: #304a67;margin: -2px;color: rgb(255,255,255);">
        <div class="container" style="max-width: 78% !important;">
            <div><a href="./index.html"><img src="assets/img/solaneos.png" style="width: 150px;" alt="logo solaneos" ></a></div>
            <div class="envelope-header" style="font-size: 30px;"></div><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1" style="background: rgb(239,84,108);"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon" style="color: rgb(255,255,255) ;"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1" style=" margin-right: -50px ;color: rgb(255,255,255);">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a class="nav-link" href="./index.html" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Accueil</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./appartements.php" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Nos appartements</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./gallery.html" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Galerie photos</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./services.html" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Nos services</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./contact.php" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Contact</strong></a></li>
                    <li class="nav-item"><button onclick="window.location.href = 'https://solaneos.amenitiz.io/fr/booking/room';" class="btn btn-primary" type="button" style="color: var(--white);border-style: none;border-color: var(--white);background: rgb(239,84,108);margin-top: 21px;margin-left: 15px; font-family: 'Josefin Sans', sans-serif; " >Réserver maintenant</button></li>
                </ul>
            </div>
        </div>
    </nav>
    <header>
        <div class="appart-slide-desktop">
            <div class="carousel slide" data-ride="carousel" id="carouselExampleIndicators">
                <div class="carousel-inner" style="height: 70vh;">
                    <div class="carousel-item active" style="margin: 0px;"><img class="w-100 d-block" src="assets/img/img_gallery/Marina_B054.jpg" alt="Slide Image" style="height: 100%;object-fit: cover;"></div>
                    <div class="carousel-item"><img class="w-100 d-block" src="assets/img/Marina_B044_B.jpg" alt="Slide Image" style="height: 80%;object-fit: cover;"></div>
                    <div class="carousel-item"><img class="w-100 d-block" src="assets/img/Marina_B048B.jpg" alt="Slide Image" style="height: 100%;object-fit: cover;"></div>
                </div>
                <div><a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" style="height: 100%;"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="height: 100%;"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
            </div>
        </div>
        <div class="appart-slide-mobile">
            <div class="carousel slide" data-ride="carousel" id="carouselExampleIndicators">
                <div class="carousel-inner" style="height: 50vh;">
                    <div class="carousel-item active" style="margin: 0px;"><img class="w-100 d-block" src="assets/img/Marina_B019-min.jpg" alt="Slide Image" style="object-fit: cover; height: 60%;"></div>
                    <div class="carousel-item"><img class="w-100  d-block" src="assets/img/Marina_B044_B.jpg" alt="Slide Image" style="object-fit: cover; height: 60%;"></div>
                    <div class="carousel-item"><img class="w-100 d-block" src="assets/img/Marina_B048B.jpg" alt="Slide Image" style="object-fit: cover; height: 60%;"></div>
                </div>
                <div><a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" style="height: 100%;"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="height: 100%;"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
            </div>
        </div>
    </header>
    <div class="row" style="margin: 0px;">
        <div class="col-sm-6 col-xl-6 offset-xl-1">
            <div class="reserver-mobile" style="text-align: right;position: fixed; width: 100%;z-index: 1; right: 0;"><button onclick="window.location.href = 'https://solaneos.amenitiz.io/fr/booking/room';" class="btn btn-primary" type="button" style=" transform: rotate(-90deg);position: sticky;background: rgb(239,84,108);border-style: none;padding: 20px 0px 20px 0px;margin: 7px -24px -50px 0px;"><i class="fa fa-calendar" style="font-size: 18px;text-align: center;padding: 5px;width: 100%;/*transform: rotate(-90deg);*/ "></i><span style="transform: rotate(-90deg);/*display: inline-block;*/">reserver</span></button></div>
            <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 35px;"><strong>Solaneos</strong></p>
            <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 20px;margin: -31px 0px 15px 0px;">Porto Vecchio, Corse (France)</p>
            <div class="description-appart-desktop">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3" style="width: 50%;">
                            <p style="font-family: 'Kaushan Script', cursive;font-size: 48px;text-align: left;color: rgb(130,165,200);margin: 0px 0px -9px 0px;">6-8</p><span><i class="far fa-user" style="color: rgb(130,165,200);"></i></span><span></span><span style="font-size: 12px;padding: 0px 0px 0px 7px;margin: 0px 0px 0px 0px;color: #304A67; font-family: 'Josefin Sans', sans-serif;"><strong>Personnes</strong></span>
                        </div>
                        <div class="col-md-3" style="width: 50%;">
                            <p style="font-family: 'Kaushan Script', cursive;font-size: 48px;text-align: left;color: rgb(130,165,200);margin: 0px 0px -9px 0px;">3</p><i class="fas fa-bed" style="color: rgb(130,165,200);"></i><span style="font-size: 12px;padding: 0px 0px 0px 7px;margin: 0px 0px 0px 0px;color: #304A67; font-family: 'Josefin Sans', sans-serif;"><strong>Chambres</strong></span>
                        </div>
                        <div class="col-md-3" style="width: 50%;">
                            <p style="font-family: 'Kaushan Script', cursive;font-size: 48px;text-align: left;color: rgb(130,165,200);margin: 0px 0px -9px 0px;">3</p><i class="fas fa-shower" style="color: rgb(130,165,200);"></i><span style="font-size: 12px;padding: 0px 0px 0px 7px;margin: 0px 0px 0px 0px;color: #304A67; font-family: 'Josefin Sans', sans-serif;"><strong>Salle de bain</strong></span>
                        </div>
                        <div class="col-md-3" style="width: 50%;">
                            <p style="font-family: 'Kaushan Script', cursive;font-size: 48px;text-align: left;color: rgb(130,165,200);margin: 0px 0px -9px 0px;">120</p><i class="fas fa-home" style="color: rgb(130,165,200);"></i><span style="font-size: 12px;padding: 0px 0px 0px 7px;margin: 0px 0px 0px 0px;color: #304A67; font-family: 'Josefin Sans', sans-serif;"><strong>Superficie en m2</strong><br></span>
                        </div>
                    </div>
                    <p style="font-size: 16px;border-style: none;border-color: rgb(138,140,143);font-family: 'Josefin Sans', sans-serif;color: rgb(111,119,128);">Superbe T4 neuf de 120m2 sur le Porto Vecchio avec terrasse couverte et jardin privatif de 130m2 comprenant : 3 grandes chambres avec 3 salles d'eau dont une suite parentale avec un vrai dressing, 1 sejour spacieux et cuisine ouverte tout équipée, espace buanderie, terrain de boule, table de ping pong et plancha, internet via box dans l'appartement...</p>
                    <p style="font-size: 16px;border-style: none;border-color: rgb(138,140,143);font-family: 'Josefin Sans', sans-serif;color: rgb(111,119,128);">Situé dans la résidence de standing (Marina Bianca) avec belle piscine, stationnement aisé et portail sécurisé, cet appartement saura vous séduire par sa situation géographique (à quelques pas du centre historique et du port de plaisance, proximité commerces et plages ) mais aussi par le confort de l'appartement (accès depuis le jardin à la piscine, tout l'équipement nécessaire pour un séjour haut de gamme). Petite copropriété car seulement une dizaine d'appartements dans la résidence !</p>
                   <a href="./gallery.html" style=" display: flex; align-items: center; "> <div style="width: 41px; height: 41px; background-color: #EF546C; color: #fff; font-weight: bold; text-align: center; line-height: 41px;">+</div> <p style="margin-left: 10px; font-weight: bold; font-family: 'Josefin Sans', sans-serif; color: #EF546C; font-size: 18px;">Plus de photos</p></a>
                </div>
            </div>
            <div class="description-appart-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3" style="width: 50%;padding: 10px 15px;"><span></span><span></span><span style="font-size: 22px;padding: 0px 7px 0px 0px;margin: 0px 0px 0px 0px;color: rgb(199,215,230); font-family: 'Kaushan Script', cursive;"><strong>6-8</strong></span><i class="far fa-user" style="color: rgb(130,165,200);"></i>
                            <p style="font-family: 'Josefin Sans', sans-serif;font-size: 16px;text-align: left;color: #304A67;margin: 0px 0px -9px 0px;"><strong>Personnes</strong></p>
                        </div>
                        <div class="col-md-3" style="width: 50%;padding: 10px 15px;"><span></span><span></span><span style="font-size: 22px;padding: 0px 7px 0px 0px;margin: 0px 0px 0px 0px;color: rgb(199,215,230);font-family: 'Kaushan Script', cursive;"><strong>3</strong></span><i class="fas fa-bed" style="color: rgb(130,165,200);"></i>
                            <p style="font-family: 'Josefin Sans', sans-serif;font-size: 16px;text-align: left;color: #304A67;margin: 0px 0px -9px 0px;"><strong>Chambres</strong></p>
                        </div>
                        <div class="col-md-3" style="width: 50%;padding: 10px 15px;"><span></span><span></span><span style="font-size: 22px;padding: 0px 7px 0px 0px;margin: 0px 0px 0px 0px;color: rgb(199,215,230);font-family: 'Kaushan Script', cursive;"><strong>3</strong></span><i class="fas fa-shower" style="color: rgb(130,165,200);"></i>
                            <p style="font-family: 'Josefin Sans', sans-serif;font-size: 16px;text-align: left;color: #304A67;margin: 0px 0px -9px 0px;"><strong>Salle de bain</strong></p>
                        </div>
                        <div class="col-md-3" style="width: 50%;padding: 10px 15px;"><span></span><span></span><span style="font-size: 22px;padding: 0px 7px 0px 0px;margin: 0px 0px 0px 0px;color: rgb(199,215,230);font-family: 'Kaushan Script', cursive;"><strong>120</strong></span><i class="fas fa-home" style="color: rgb(130,165,200);"></i>
                            <p style="font-family: 'Josefin Sans', sans-serif;font-size: 16px;text-align: left;color: #304A67;margin: 0px 0px -9px 0px;"><strong>Superficie en m2</strong></p>
                        </div>
                    </div>
                </div>
                <p style="font-size: 14px;border-style: none;border-color: rgb(138,140,143);font-family: 'Josefin Sans', sans-serif;color: rgb(111,119,128); margin-bottom: 0px;">Superbe T4 neuf de 120m2 sur le Porto Vecchio avec terrasse couverte et jardin privatif de 130m2 comprenant : 3 grandes chambres avec 3 salles d'eau dont une suite parentale avec un vrai dressing, 1 sejour spacieux et cuisine ouverte tout équipée, espace, buanderie, terrain de boule, table de ping pong et plancha, internet via box dans l'appartement...</p>
                <p  style="margin: 0px; text-align: center;">
                    
                    <a href="javascript:document.getElementById('re001').style.display='block';document.getElementById('re001');" style="color: #EF546C;font-size: 14px;">
                    <u>Lire la suite</u></a>
                </p>
                <div id="re001" style="display: none;">
                <p style="font-size: 14px;border-style: none;border-color: rgb(138,140,143);font-family: 'Josefin Sans', sans-serif;color: rgb(111,119,128);">Situé dans la résidence de standing (Marina Bianca) avec belle piscine, stationnement aisé et portail sécurisé, cet appartement saura vous déduire par sa situation géographique (à quelques pas du centre historique et du port de plaisance, proximité commerces et plages ) mais aussi par le confort de l'appartement (accès depuis le jardin à la piscine, tout l'équipement nécessaire pour un séjour haut de gamme). Petite copropriété car seulement une dizaine d'appartements dans la résidence !</p>
                <a href="./gallery.html" style=" display: flex; align-items: center; "> <div style="width: 41px; height: 41px; background-color: #EF546C; color: #fff; font-weight: bold; text-align: center; line-height: 41px;">+</div> <p style="margin-left: 10px; font-weight: bold; font-family: 'Josefin Sans', sans-serif; color: #EF546C; font-size: 18px;">Plus de photos</p></a> <br><br>
                <a href="javascript:document.getElementById('re001').style.display='none';document.getElementById('re001');" style="color: #EF546C;font-size: 14px;">
                    <u>FERMER</u></a>
                </div>
            </div>
            <hr style="color: var(--purple);border-width: 1px;border-color: var(--pink);">
            <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 29px;"><strong>Services disponibles</strong></p>
            <div class="services-dispo-desktop">
                
                <div class="container" style="margin: 0px 0px 10px 0px;">
                    <div class="row">
                        <div class="col-md-5"> <a href="./services.html"><img  src="assets/img/menage.jpg" alt="image services ménage"><span style="font-size: 14px;font-family: 'Josefin Sans', sans-serif;padding: 0px 0px 0px 8px;color: rgb(116,121,126); width: 260px !important;">Ménage</span></a> </div>
                        <!-- <div class="col-md-4"><img src="assets/img/lit_bebe.jpg"><span style="font-size: 14px;font-family: 'Josefin Sans', sans-serif;padding: 0px 0px 0px 8px;color: rgb(116,121,126);">Lit bébé</span></div> -->
                        <div class="col-md-5"> <a href="./services.html"> <img src="assets/img/garde_enfants.jpg" alt="image garde d'enfants"><span style="font-size: 14px;font-family: 'Josefin Sans', sans-serif;padding: 0px 0px 0px 3px;color: rgb(116,121,126); width: 260px !important;">Garde d'enfants</span></a></div>
                    </div>
                </div>
                <div class="container" style="padding: 0px 0px 30px 15px;">
                    <div class="row">
                        <div class="col-md-5"> <a href="./services.html"> <img src="assets/img/locatio_bateau.jpg" alt="image location de bateau"><span style="font-size: 14px;font-family: 'Josefin Sans', sans-serif;padding: 0px 0px 0px 8px;color: rgb(116,121,126); width: 260px !important;">Location de bateau</span></a></div>
                        <div class="col-md-5"> <a href="./services.html"> <img src="assets/img/conciergerie.jpg" alt="image conciergerie"><span style="font-size: 14px;font-family: 'Josefin Sans', sans-serif;padding: 0px 0px 0px 8px;color: rgb(116,121,126); width: 260px !important;">Conciergerie</span></a></div>
                    </div>
                </div>
            
            </div>
            <div class="services-dispo-mobile" style="padding-bottom: 15px;">
                <div class="container" style="margin: 0px 0px 10px 0px;">
                    <div class="row">
                         <div class="col-md-5" style="width: 100%;"><i class="fa fa-circle" style="font-size: 8px;"></i><span style="font-size: 14px;padding: 0px 0px 0px 10px;">Ménage</span></div>
                        <!-- <div class="col-md-4" style="width: 50%;"><i class="fa fa-circle" style="font-size: 8px;"></i><span style="font-size: 14px;padding: 0px 0px 0px 10px;">Lit bébé</span></div> -->
                        <div class="col-md-5" style="width: 100%;"><i class="fa fa-circle" style="font-size: 8px;"></i><span style="font-size: 14px;padding: 0px 0px 0px 10px;">Location de bateau</span></div>
                        <div class="col-md-5" style="width: 100%;"><i class="fa fa-circle" style="font-size: 9px;"></i><span style="font-size: 14px;padding: 0px 0px 0px 10px;">Conciergerie</span></div>
                        <div class="col-md-5" style="width: 100%;"><i class="fa fa-circle" style="font-size: 8px;"></i><span style="font-size: 14px;padding: 0px 0px 0px 10px;">Garde d'enfants</span></div>
                    </div>
                </div>
            </div>
            <a href="./services.html"> <div style="font-size: 18px;font-family: 'Josefin Sans', sans-serif;color: rgb(239,84,108); display : flex; align-items: center;"> <div style="color: var(--white);background: rgb(239,84,108);border-style: none;padding: 10px 15px; display:flex; justify-content : center; align-items: center; width: 45px; height: 45px"><i class="fa fa-chevron-right"></i></div><strong id="plus_info" style="text-decoration: none; color: #EF546C;">&nbsp;Plus d'informations&nbsp;</strong></div> </a>  
            <hr style="color: var(--purple);border-width: 1px;border-color: var(--pink);">
            <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 29px;"><strong>Pièces</strong></p>
            <div class="piece-desktop">
                <div class="container">
                    <div class="row" style ="z-index: 0;">
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px;  box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 237px; border-radius: 20px;  font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Suite parentale  <br> (Chambre 1)</strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Lit king size</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Salles de bain et WC</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Dressing</p>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px;margin-left: 10px; box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 237px; border-radius: 20px; font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Chambre 2</strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Lit king size</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Accès direct au jardin</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Salle de bain</p>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px; margin-left: 10px; box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 237px; border-radius: 20px; font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Chambre 3</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">2 lits simples</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Salle de bain et WC<br></p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="margin: 0px 0px 18px 0px;padding: 5px;">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px;width:  100%; box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 237px; border-radius: 20px; font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Salon</strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Canapé convertible</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Table basse</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Grande table à manger</p>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px; margin-left: 10px; min-height: 237px; box-shadow: 0px 0px 8px rgb(201, 201, 201); border-radius: 20px; font-family: 'Josefin Sans', sans-serif;  display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67;  font-style: italic; margin: 6px 0 !important;"><strong>Cuisine</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Equipement complet</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Four + Micro-ondes</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Lave-vaisselle</p>
                                
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                            <div style="font-family: 'Josefin Sans', sans-serif; margin-left: 10px; display: flex; flex-direction: column; box-shadow: 0px 0px 8px rgb(201, 201, 201); align-items: center; justify-content: space-around; background: #e4eff5;padding: 10px 0px 10px 0px; border-radius: 20px; min-height: 237px;">
                                <p style=" text-align: center;font-size: 23px;color: #304A67;  font-style: italic; margin: 6px 0 !important;"><strong>Buanderie</strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Rangements</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Fer + Planche à repasser</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Aspirateur</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Machine à laver</p>
                                
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px; box-shadow: 0px 0px 8px rgb(201, 201, 201); font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;border-radius: 20px;min-height: 237px;">
                                <p style="text-align: center;font-size: 23px;margin: 0px 0px 20px 0px;color: #304A67; font-style: italic; margin: 6px 0 !important; "><strong>Terasse</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Plancha</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Table à manger</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Espace aménagé<br></p>
                                
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px; margin-left: 10px; box-shadow: 0px 0px 8px rgb(201, 201, 201); font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;border-radius: 20px;min-height: 237px;">
                                <p style="text-align: center;font-size: 23px;margin: 0px 0px 20px 0px;color: #304A67; font-style: italic; margin: 6px 0 !important; "><strong>Jardin</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Terrain de pétanque</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Transats</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Parasol</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Table de ping pong</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Salon de jardin</p>
                                
                            </div>

                        </div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;"></div>
                    </div>
                </div>
            </div>
            <div class="piece-mobile">
                 <main>
                    <div class="carousel">
                        <ul class="carousel-items">
                            <li class="carousel-item-piece">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px;  box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 230px; border-radius: 20px;  font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Suite parentale <br> (Chambre 1) </strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Lit king size</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Salles de bain et WC</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Dressing</p>
                            </div>
                            </li>
                            <li class="carousel-item-piece">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px;margin-left: 10px; box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 230px; border-radius: 20px; font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Chambre 2</strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Lit king size</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Accès direct au jardin</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Salle de bain</p>
                            </div>
                            </li>
                            <li class="carousel-item-piece">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px; margin-left: 10px; box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 230px; border-radius: 20px; font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Chambre 3</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">2 lits simples</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Salle de bain et WC<br></p>
                                
                            </div>
                            </li>
                            <li class="carousel-item-piece">
                               
                                <div style="background: #e4eff5;padding: 10px 0px 10px 0px;width:  100%; box-shadow: 0px 0px 8px rgb(201, 201, 201); min-height: 230px; border-radius: 20px; font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67; font-style: italic; margin: 6px 0 !important;"><strong>Salon</strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Canapé convertible</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Table basse</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">TV avec Chromecast</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Grande table à manger</p>
                            </div>
                               
                            </li>
                            <li class="carousel-item-piece">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px; margin-left: 10px; min-height: 230px; box-shadow: 0px 0px 8px rgb(201, 201, 201); border-radius: 20px; font-family: 'Josefin Sans', sans-serif;  display: flex; flex-direction: column; align-items: center; justify-content: space-around;">
                                <p style="text-align: center;font-size: 23px;color: #304A67;  font-style: italic; margin: 6px 0 !important;"><strong>Cuisine</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Equipement complet</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Four + Micro-ondes</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Lave-vaisselle</p>
                                
                            </div>
                            </li>
                            <li class="carousel-item-piece">
                            <div style="background: #e4eff5;padding: 10px 0px 10px 0px; box-shadow: 0px 0px 8px rgb(201, 201, 201); font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;border-radius: 20px;min-height: 230px;">
                                <p style="text-align: center;font-size: 23px;margin: 0px 0px 20px 0px;color: #304A67; font-style: italic; margin: 6px 0 !important; "><strong>Terasse</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Plancha</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Table à manger</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Espace aménagé<br></p>
                                
                            </div>
                            </li>
                            <li class="carousel-item-piece">
                                <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px;">
                                <div style="font-family: 'Josefin Sans', sans-serif; margin-left: 10px; display: flex; flex-direction: column; box-shadow: 0px 0px 8px rgb(201, 201, 201); align-items: center; justify-content: space-around; background: #e4eff5;padding: 10px 0px 10px 0px; border-radius: 20px; min-height: 230px;">
                                <p style=" text-align: center;font-size: 23px;color: #304A67;  font-style: italic; margin: 6px 0 !important;"><strong>Buanderie</strong></p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Rangements</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Fer + Planche à repasser</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Aspirateur</p>
                                <p style="text-align: center;margin: 0px;font-size: 16px;color: rgb(138,140,143);">Machine à laver</p>
                                
                            </div>
                                </div>
                            </li>
                            <li  class="carousel-item-piece">
                                <div class="col-md-4" style="margin: 0px 0px 20px 0px;padding: 5px; ">
                                <div style="background: #e4eff5;padding: 10px 0px 10px 0px; margin-left: 10px; box-shadow: 0px 0px 8px rgb(201, 201, 201); font-family: 'Josefin Sans', sans-serif; display: flex; flex-direction: column; align-items: center; justify-content: space-around;border-radius: 20px;min-height: 230px;">
                                <p style="text-align: center;font-size: 23px;margin: 0px 0px 20px 0px;color: #304A67; font-style: italic; margin: 6px 0 !important; "><strong>Jardin</strong></p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Terrain de pétanque</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Transats</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Parasol</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Table de ping pong</p>
                                <p style="text-align: center;margin: 5px;font-size: 16px;color: rgb(138,140,143);">Salon de jardin</p>
                                
                            </div>
        
                                </div>
                            </li>
                        </ul>
</div>
                </main>
            </div>
            <hr style="color: var(--purple);border-width: 1px;border-color: var(--pink);">
            <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 29px;"><strong>Equipements</strong></p>
            <div class="equipements-desktop">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Draps et linges inclus</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">WIFI gratuit</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Piscine</span></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Dressing</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Parking gratuit</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Vue mer</span></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Téléviseur</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Jardin privatif de 130m2</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);text-align: center;">Lave linge</span></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Cuisine</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Salon de jardin</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);text-align: center;">Lave vaisselle</span></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xl-5" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Table de ping pong</span></div>
                        <div class="col-md-4 col-xl-5" style="margin: 8px 0px 20px -88px;text-align: left;padding: 5px;" id="petanque"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Terrain de pétanque</span></div>
                        
                    </div>
                </div>
            </div>
            <div class="equipements-mobile" style="text-align: center;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Draps et linges inclus</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">WIFI gratuit</span></div>
                        <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Piscine</span></div>
                    </div>
                </div>
                <a id="afficherplus" href="javascript:document.getElementById('re002').style.display='block';document.getElementById('re002');" onclick=" this.style.display = 'none';">
                    <button class="btn btn-primary" type="button" style="background: rgb(239,84,108);border-style: none;"><span>Afficher les 10 équipements<i class="fa fa-chevron-down" style="padding: 5px;"></i></span></button>
                </a>
                <div id="re002" style="display: none;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Dressing</span></div>
                            <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Parking gratuit</span></div>
                            <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Vue mer</span></div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Téléviseur</span></div>
                            <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Jardin privatif de 130m2</span></div>
                            <div class="col-md-4" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);text-align: center;">Lave linge</span></div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-xl-5" style="margin: 0px 0px 20px 0px;text-align: left;padding: 5px;"><i class="fa fa-check" style="color: rgb(239,84,108);"></i><span style="padding: 0px 0px 0px 5px;font-family: 'Josefin Sans', sans-serif;font-size: 18px;color: rgb(104,106,108);">Cuisine entièrement équipée</span></div>
                            
                        </div>
                    </div>
                    <a href="javascript:document.getElementById('re002').style.display='none';document.getElementById('re002');" onclick="afficherplusservice()">
                        <button class="btn btn-primary" type="button" style="background: rgb(239,84,108);border-style: none;"><span>Afficher moins<i class="fa fa-chevron-up" style="padding: 5px;"></i></span></button>
                    </a>
                </div>
            </div>
            <hr style="color: var(--purple);border-width: 1px;border-color: var(--pink);">
            <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 29px;text-align: left;"><strong>Autour du logement</strong></p>
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="padding: 0px 0px 10px 15px;"><span style="color: rgb(104,106,108);font-size: 23px; font-family: 'Josefin Sans', sans-serif;"><i class="fa fa-map-marker" style="color: rgb(239,84,108);padding: 0px 10px 0px 0px; "></i>Porto Vecchio, Corse</span></div>
                </div>
            </div>
            <section class="map-clean"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1254.527174121947!2d9.280501177218055!3d41.595527623022605!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12d983fd9ea95e8d%3A0xfc72311f0faa99fc!2sSOLANEOS!5e0!3m2!1sfr!2sfr!4v1618908078515!5m2!1sfr!2sfr"  height="450" style="border:0; width: 100%;" allowfullscreen="" loading="lazy"></iframe></section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 23px;">À proximité</p><span style="color: rgb(104,106,108);font-size: 18px; font-family: 'Josefin Sans', sans-serif;"><i class="fa fa-bus" style="color: #304A67;padding: 0px 10px 0px 0px;"></i>Navette électrique</span>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="padding: 10px 15px;"><span style="color: rgb(104,106,108);font-size: 18px; font-family: 'Josefin Sans', sans-serif;"><i class="fas fa-umbrella-beach" style="color: #304A67;padding: 0px 10px 0px 0px;"></i>Plage à proximité</span></div>
                    <div class="col-md-12" style="padding: 10px 15px;"><span style="color: rgb(104,106,108);font-size: 18px; font-family: 'Josefin Sans', sans-serif;"><i class="fas fa-store" style="color: #304A67;padding: 0px 10px 0px 0px;"></i>Marché local à proximité (Dimanche)</span></div>
                    <div class="col-md-12" style="padding: 10px 15px;"><span style="color: rgb(104,106,108);font-size: 18px; font-family: 'Josefin Sans', sans-serif;"><i class="fa fa-anchor" style="color: #304A67;padding: 0px 10px 0px 0px;"></i>Le port de plaisance</span></div>
                    <div class="col-md-12" style="padding: 10px 15px;"><span style="color: rgb(104,106,108);font-size: 18px; font-family: 'Josefin Sans', sans-serif;"><i class="fas fa-city" style="color: #304A67;padding: 0px 10px 0px 0px;"></i>Centre ville</span></div>
                </div>
            </div>
            <hr style="color: var(--purple);border-width: 1px;border-color: var(--pink);">
            <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 29px;"><strong>Avis clients</strong></p><!-- Slideshow container -->
            <div class="slideshow-container">


                <div class="commentaire_section" style="width: 100%; max-height: 700px; padding-bottom: 200px; ">
                    <div class="commentaire_card" style="width: 100%; height: auto;">
                        <div class="nom_prenom" style="width: 90%; height: 50px ; display: flex; align-items: center; ">
                                <p style="padding-left: 20px; padding-right: 20px; font-size: 18px; font-family: 'Josefin Sans', sans-serif;">Michel Mariotte</p><div style="background-color: #EF546C; width: 15px; height: 15px; border-radius: 50%;"></div><p style="padding-left: 20px; font-size: 18px; font-family: 'Josefin Sans', sans-serif;">Il y a 2 jours</p>
                        </div>
                        <div class="message_comment" style="width: 100%; height: auto; padding-left: 20px; padding-right: 20px; border-bottom: solid 1px rgb(104,106,108) ;">
                            <p style=" font-family: 'Josefin Sans', sans-serif; text-align: justify; color: rgb(104,106,108);">Appartement neuf et moderne, très confortable et très bien situé. La réponse de Dumenica a toujours été rapide et attentive, et ils nous ont fourni tout ce dont nous avions besoin.</p>
                        </div>
                    </div>
                </div>
               
                <form action="multi_form_action_avis.php" method="post" style="width: 100%; height: auto; padding-bottom: 100px;" id="avis">

                    <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;font-size: 29px; "><strong>Laisser un commentaire</strong></p>

                    <div class="star">
                        <label for="star">
                        <input class="star star-5" id="star-5" type="radio" name="star" value="5"/> <label class="star star-5" for="star-5"></label> 
                        <input class="star star-4" id="star-4" type="radio" name="star" value="4"/> <label class="star star-4" for="star-4"></label> 
                        <input class="star star-3" id="star-3" type="radio" name="star" value="3"/> <label class="star star-3" for="star-3"></label>
                        <input class="star star-2" id="star-2" type="radio" name="star" value="2"/> <label class="star star-2" for="star-2"></label> 
                        <input class="star star-1" id="star-1" type="radio" name="star" value="1" /> <label class="star star-1" for="star-1"></label> 
                        <label>
                    </div>
                    
                   <div><label style="margin-top: 20px; margin-left: 10px; font-family: 'Josefin Sans', sans-serif; color: #304A67; font-weight: bold;">Nom <span style="color: #EF546C; font-size: 25px;">*</span> <br><input type="text"  id="nom" name="nom" style="font-size: 18px; font-family: 'Josefin Sans', sans-serif; height: 40px; padding-left: 10px; border-radius: 8px; border: solid 1px rgb(104,106,108); color: rgb(104,106,108);" required >  </label>

                   <label style="margin-top: 20px; margin-left: 10px; font-family: 'Josefin Sans', sans-serif; color: #304A67; font-weight: bold;">Prénom <span style="color: #EF546C; font-size: 25px;">*</span> <br> <input type="text"  id="prenom" name="prenom" style="font-size: 18px; font-family: 'Josefin Sans', sans-serif; height: 40px; padding-left: 10px; border-radius: 8px; border: solid 1px rgb(104,106,108); color: rgb(104,106,108);" required >   </label>

                </div> <br>
                   <label for="commentaire" style="margin-top: 20px; margin-left: 10px; font-family: 'Josefin Sans', sans-serif; color: #304A67; font-weight: bold;">Message <span style="color: #EF546C; font-size: 25px;">*</span></label> <textarea name="commentaire" id="commentaire" style="width: 100%; min-height: 300px; font-family: 'Josefin Sans', sans-serif; height: 40px; padding-left: 10px; border-radius: 8px; border: solid 1px rgb(104,106,108) ; color: rgb(104,106,108);" required></textarea>
                    <input type="submit" name="submit" value="Valider" style="width: 200px; height: 50px; position: relative; float: right; margin-top: 30px; border: none; background-color: #EF546C; color: #ffffff; ">

                </form>
                <?php if (isset($_SESSION['success_message']) && !empty($_SESSION['success_message'])) { ?>
                        <div class="success-message" style="margin-bottom: 20px;font-size: 20px;color: white ;background-color: #EF546C;text-align: center;padding: 10px;"><?php echo $_SESSION['success_message']; ?></div>
                        <?php
                        unset($_SESSION['success_message']);
                    }
                 ?>

    
            </div>
            <br>
        </div>




        <div class="col-sm-6 col-xl-4">
            <div class="reserver-desktop" style="position: sticky;top: 26px;box-shadow: 0px 0px 11px rgb(122,122,122);margin: -150px 0px 0px 50px;width: 80%;">
                <div style="background: rgb(239,84,108);">
                    <p style="font-family: 'Josefin Sans', sans-serif;color: rgb(255,255,255);font-size: 28px;margin: 0px 0px 0px 0px;padding: 20px 0px 0px 20px;">Appartement Porto Vecchio</p>
                    <p style="font-family: 'Josefin Sans', sans-serif;color: rgb(255,255,255);font-size: 22px;margin: 0px;padding: 0px 0px 20px 20px;">à partir de 1250€ / Semaine</p>
                </div>
                <div style="text-align: left;background: #ffffff;">
                    <form>
                       
                        <div class="col-auto" style="padding: 0px 15px 0px 15px;">
                            
                        </div>
                        <div class="container">
                            <div class="form-row">
                               
                            </div>
                        </div>
                        <div style="text-align: center;padding: 20px 0px 10px 0px;"><button onclick="window.location.href = 'https://solaneos.amenitiz.io/fr/booking/room';" class="btn btn-primary" type="button" style="text-align: center;background: #304A67;">réserver</button></div>
                    </form>
                    <div style="text-align: center;padding: 0px 0px 10px 0px;">
                        <hr style="color: var(--gray);width: 90%;border-width: 1px;border-color: #717171;">
                        <p style="font-family: 'Josefin Sans', sans-serif;color: rgb(239,84,108);margin: 10px 0px;"><strong>Contactez-nous directement</strong></p>
                        <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;margin: 10px 0px;">solaneoslocations@gmail.com</p>
                        <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;margin: 10px 0px;">06 32 82 69 59</p>
                        <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;margin: 10px 0px;">06 03 01 11 17</p>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <footer id="myFooter" style="background: #304A67;  padding-bottom: 1px !important ;">
        <div class="container-fluid">
            <div class="row text-center" style="font-family: 'Josefin Sans', sans-serif;">
                <div class="col-12 col-sm-6 col-md-3">
                    <h1 class="logo" style="margin-top: 30px;"><img src="assets/img/solaneos.png" alt="logo solaneos" style="width: 150px;"></h1>
                </div>
                <div class="col-12 col-sm-6 col-md-2" style="padding: 30px 15px;">
                    <ul >
                        <li style="color: rgb(255,255,255); "><a href="./appartements.php" style="font-size: 16px;color: white;">Nos appartements</a></li>
                        <li style="color: rgb(255,255,255); "><a href="./services.html" style="font-size: 16px;color: white;">Nos services<br></a></li>
                        <li style="color: rgb(255,255,255); "><a href="./contact.php" style="font-size: 16px;color: white;">Contact</a></li>
                        <li style="color: rgb(255,255,255); "><a href="./cgv.html" style="font-size: 16px;color: white;">CGV</a></li>
                    </ul>
                </div>
               
            </div>
        </div>
    </footer>

    <style>
                        
                        div.stars {
                                width: 270px;
                                display: inline-block
                            }

                            .mt-200 {
                                margin-top: 200px
                            }

                            input.star {
                                display: none
                            }

                            label.star {
                                float: right;
                                padding: 10px;
                                font-size: 22px;
                                color: #304A67;
                                transition: all .2s
                            }

                            input.star:checked~label.star:before {
                                content: '\f005';
                                color: #FD4;
                                transition: all .25s
                            }

                            input.star-5:checked~label.star:before {
                                color: #FE7;
                                text-shadow: 0 0 20px #952
                            }

                            input.star-1:checked~label.star:before {
                                color: #F62
                            }

                            label.star:hover {
                                transform: rotate(-15deg) scale(1.3)
                            }

                            label.star:before {
                                content: '\f006';
                                font-family: FontAwesome
                            }
                        
                        
                        </style>


    


    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/clean-blog.js"></script>
    <script src="assets/js/DateRangePicker.js"></script>
    <script src="assets/js/Fixed-navbar-starting-with-transparency.js"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>
    <script src="assets/js/Navbar---Apple.js"></script>
    <script src="assets/js/responsive-blog-card-slider-1.js"></script>
    <script src="assets/js/responsive-blog-card-slider.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
    <script src="assets/js/Slider_Carousel_webalgustocom.js"></script>
    <script src="assets/js/slideshow_w3css-mloureiro1973.js"></script>
    <script src="assets/js/Slideshow-ML-de-3-Slides.js"></script>
    <script src="assets/js/Swiper-Slider-Card-For-Blog-Or-Product.js"></script>
    <script src="assets/js/Swiper-Slider.js"></script>
    <script>
        function afficherplusservice() {
          document.getElementById("afficherplus").style.display = "block";
        }
    </script>

</body>

</html>