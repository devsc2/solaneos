<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Si vous avez une question sur une des locations ou des services disponibles, n'hésitez pas à contacter Solaneos via notre numéro de téléphone ou mail ou directement via le formulaire de contact.">
    <title>Contact</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Slab:300,400">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/Fixed-navbar-starting-with-transparency-1.css">
    <link rel="stylesheet" href="assets/css/Fixed-navbar-starting-with-transparency.css">
    <link rel="stylesheet" href="assets/css/Bold-BS4-Footer-Big-Logo.css">
    <link rel="stylesheet" href="assets/css/Customizable-Background--Overlay.css">
    <link rel="stylesheet" href="assets/css/Dark-NavBar-1.css">
    <link rel="stylesheet" href="assets/css/Dark-NavBar-2.css">
    <link rel="stylesheet" href="assets/css/Dark-NavBar.css">
    <link rel="stylesheet" href="assets/css/gradient-navbar-1.css">
    <link rel="stylesheet" href="assets/css/gradient-navbar.css">
    <link rel="stylesheet" href="assets/css/MUSA_carousel-product-cart-slider-1.css">
    <link rel="stylesheet" href="assets/css/MUSA_carousel-product-cart-slider.css">
    <link rel="stylesheet" href="assets/css/Navbar---Apple-1.css">
    <link rel="stylesheet" href="assets/css/Navbar---Apple.css">
    <link rel="stylesheet" href="assets/css/slideshow_w3css-mloureiro1973-1.css">
    <link rel="stylesheet" href="assets/css/slideshow_w3css-mloureiro1973.css">
    <link rel="stylesheet" href="assets/css/sticky-dark-top-nav-with-dropdown.css">
    <link rel="stylesheet" href="assets/css/Tricky-Grid---2-Column-on-Desktop--Tablet-Flip-Order-of-12-Column-rows-on-Mobile.css">
    <link rel="stylesheet" href="assets/css/footerResponcive.css">
    
</head>

<body style="overflow-x : hidden !important">
    
    <div class="contactheader">
        <div style="text-align: right;padding: 10px 10% 10px 0px;background: #e4eff5;color: #304a67;font-size: 16px;font-family: 'Josefin Sans', sans-serif;"><i class="fa fa-phone" style="padding: 0px 10px 0px 50px;"></i><span><strong>06 32 82 69 59 | 06 03 01 11 17</strong></span><i class="fa fa-envelope" style="padding: 0px 10px 0px 50px;"></i><span style="font-size: 16px;"><strong>solaneoslocations@gmail.com</strong></span></div>
        <hr style="margin: -1px;border-width: 5px;border-color: rgb(239,84,108);">
</div>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top navbar-transparency" style="position: relative;background: #304a67;margin: -2px;color: rgb(255,255,255);">
        <div class="container" style="max-width: 83% !important;">
            <div><a href="./index.html"><img src="assets/img/solaneos.png" alt="logo solaneos" style="width: 150px;"></a></div>
            <div class="envelope-header" style="font-size: 30px;"></div><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1" style="background: rgb(239,84,108);"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon" style="color: rgb(255,255,255);"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1" style="color: rgb(255,255,255);">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="./index.html" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Accueil</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./appartements.php" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Nos appartements</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./gallery.html" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Galerie photos</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./services.html" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Nos services</strong></a></li>
                    <li class="nav-item"><a class="nav-link" href="./contact.php" style="color: #ffffff;font-family: 'Josefin Sans', sans-serif;padding: 0px 15px;"><strong>Contact</strong></a></li>
                    <li class="nav-item"><button onclick="window.location.href = 'https://solaneos.amenitiz.io/fr/booking/room';" class="btn btn-primary" type="button" style="color: var(--white);border-style: none;border-color: var(--white);background: rgb(239,84,108);margin-top: 21px;margin-left: 5px;font-weight: bold;">Réserver maintenant</button></li>
                </ul>
            </div>
        </div>
    </nav>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xl-8" style="margin: 0px 0px 30px 0px;">
                    <p style="text-align: center;font-family: 'Kaushan Script', cursive;font-size: 62px;color: #e4eff5;border-color: rgb(150,150,150);">Questions ?</p>
                    <p style="text-align: center;font-family: 'Josefin Sans', sans-serif;font-size: 30px;margin: -72px 0px 0px -2px;padding: 5px;color: rgb(2,49,95);border-color: #304A67;"><strong>Nous contacter</strong></p>
                    <hr style="border-color: var(--pink);">
                    <?php if (isset($_SESSION['success_message']) && !empty($_SESSION['success_message'])) { ?>
                        <div class="success-message" style="margin-bottom: 20px;font-size: 20px;color: green;"><?php echo $_SESSION['success_message']; ?></div>
                        <?php
                        unset($_SESSION['success_message']);
                    }
                    ?>             
                     <form method="POST" action="multi_form_action.php">
                        <div class="form-row">
                            <div class="col-sm-6">
                                <p style="font-size: 16px;color: #304A67;margin: 5px 0px; font-family: 'Josefin Sans', sans-serif;"><strong>Nom</strong></p><input class="form-control" type="text" id="nom" name="nom" required>
                            </div>
                            <div class="col-sm-6">
                                <p style="font-size: 16px;color: #304A67;margin: 5px 0px; font-family: 'Josefin Sans', sans-serif;"><strong>Prénom</strong></p><input class="form-control" type="text" id="prenom" name="prenom" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <p style="font-size: 16px;color: #304A67;margin: 5px 0px; font-family: 'Josefin Sans', sans-serif;"><strong>Email</strong></p><input class="form-control" type="email" id="email" name="email" placeholder="exemple@gmail.com" required>
                            </div>
                            <div class="col-sm-6">
                                <p style="font-size: 16px;color: #304A67;margin: 5px 0px; font-family: 'Josefin Sans', sans-serif;"><strong>Numéro de téléphone</strong></p><input class="form-control" type="tel" id="tel" name="tel" required>
                            </div>
                        </div>
                        <div>
                            <p style="font-size: 16px;color: #304A67;margin: 5px 0px; font-family: 'Josefin Sans', sans-serif;" ><strong>Message</strong></p><textarea class="form-control" id="messag" name="messag" placeholder="Une requête en particulier ?"></textarea>
                        </div>
                        <div style=" display: flex; justify-content: space-around; align-items: center; flex-wrap: wrap;"><button class="btn btn-primary" id="submit" type="submit" name="submit" style="background: #304a67;margin: 30px 0px 30px 0px;"><strong>Envoyer</strong></button>
                    </div>
                       
                    </form>
                    <!-- solaneoslocations@gmail.com -->
                </div>
                <div class="col">
                    <div style="position: sticky;top: 106px;box-shadow: 0px 0px 11px rgb(122,122,122);margin: 0px 0px 0px 20px;width: 90%;">
                        <div style="background: rgb(239,84,108);">
                            <p style="font-family: 'Josefin Sans', sans-serif;color: rgb(255,255,255);font-size: 28px;margin: 0px 0px 0px 0px;padding: 20px 0px 10px 0px;text-align: center;"><strong>Solaneos&nbsp;</strong></p>
                        </div>
                        <div style="text-align: left;background: #ffffff;">
                            <div style="text-align: center;padding: 0px 0px 30px 0px;">
                                <hr style="color: var(--gray);width: 90%;border-width: 1px;border-color: #717171;">
                                <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;">solaneoslocations@gmail.com</p>
                                <p style="font-family: 'Josefin Sans', sans-serif;color: #304A67;">06 32 82 69 59 | 06 03 01 11 17</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                </div>
    <footer id="myFooter" style="background: #304A67;padding: 10px 0px 1px; width: 100%; bottom: 0; min-height: 150px ; padding-bottom: 1px !important ; ">
        <div class="container-fluid">
            <div class="row text-center" style="font-family: 'Josefin Sans', sans-serif; margin: 0;">
                <div class="col-12 col-sm-6 col-md-3">
                    <h1 class="logo" style="margin-top: 30px;"><img alt="solaneos" src="assets/img/solaneos.png" style="width: 150px;"></h1>
                </div>
                <div class="col-12 col-sm-6 col-md-2" style="padding: 30px 15px;">
                    <ul>
                        <li style="color: rgb(255,255,255);"><a href="./appartements.php" style="font-size: 16px;color: white;">Nos appartements</a></li>
                        <li style="color: rgb(255,255,255);"><a href="./services.html" style="font-size: 16px;color: white;">Nos services<br></a></li>
                        <li style="color: rgb(255,255,255);"><a href="./contact.html" style="font-size: 16px;color: white;">Contact</a></li>
                        <li style="color: rgb(255,255,255);"><a href="./cgv.html" style="font-size: 16px;color: white;">CGV</a></li>
                    </ul>
                </div>
                <!-- <div class="col-12 col-sm-6 col-md-2" style="padding: 30px 15px;">
                    <ul>
                        
                        <li></li>
                    </ul>
                </div> -->
            </div>
        </div>
    </footer>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/clean-blog.js"></script>
    <script src="assets/js/Fixed-navbar-starting-with-transparency.js"></script>
    <script src="assets/js/Navbar---Apple.js"></script>
    <script src="assets/js/slideshow_w3css-mloureiro1973.js"></script>
</body>

</html>