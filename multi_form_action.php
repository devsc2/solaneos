<?php
include_once("db_connect.php");

if (isset($_POST['submit'])) {

	$entete  = 'MIME-Version: 1.0' . "\r\n";
	$entete .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$entete .= 'From: ' . $_POST['email'] . "\r\n";

	$contenue = '<h1>Message envoyé depuis la page Contact</h1>
	<p><b>Nom : </b>' . $_POST['nom'] . '<br>
	<b>Prénom : </b>' . $_POST['prenom'] . '<br>
	<b>Email : </b>' . $_POST['email'] . '<br>
	<b>Téléphone : </b>' . $_POST['tel'] . '<br>
	<b>Message : </b>' . $_POST['messag'] . '</p>';

	$retour = mail('solaneoslocations@gmail.com', 'Envoi depuis page Contact', $contenue, $entete);

	$stmt = $db->prepare('INSERT INTO info(nom, prenom, email, tel, messag) VALUES (:nom, :prenom, :email, :tel, :messag)');
	$stmt->bindParam(':nom', $nom);
	$stmt->bindParam(':prenom', $prenom);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':tel', $tel);
	$stmt->bindParam(':messag', $messag);

	$nom = htmlspecialchars($_POST['nom']);
	$prenom = htmlspecialchars($_POST['prenom']);
	$email = htmlspecialchars($_POST['email']);
	$tel = htmlspecialchars($_POST['tel']);
	$messag = htmlspecialchars($_POST['messag']);

	if (!(!$nom || !$prenom || !$email || !$tel || !$messag )) {
		$stmt->execute();
		session_start();
		$_SESSION['success_message'] = "Votre message à été envoyé.";
		header("location:contact.php");
		exit();
	}
}
