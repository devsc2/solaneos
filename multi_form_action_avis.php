<?php
include_once("db_connect.php");

if (isset($_POST['submit'])) {

	$entete  = 'MIME-Version: 1.0' . "\r\n";
	$entete .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$entete .= 'From: ' . $_POST['email'] . "\r\n";

	$contenue = '<h1>Nouvelle avis sur le site</h1>
	<p><b>Etoiles : </b>' . $_POST['star'] . '<br>
	<b>Nom : </b>' . $_POST['nom'] . '<br>
	<b>Prénom : </b>' . $_POST['prenom'] . '<br>
	<b>Avis : </b>' . $_POST['commentaire'] .'</p>';

	$retour = mail('solaneoslocations@gmail.com', 'Envoi depuis page Contact', $contenue, $entete);

	$stmt = $db->prepare('INSERT INTO avis(etoiles, nom, prenom, commentaire) VALUES (:star, :nom, :prenom, :commentaire)');
	$stmt->bindParam(':star', $star);
	$stmt->bindParam(':nom', $nom);
	$stmt->bindParam(':prenom', $prenom);
	$stmt->bindParam(':commentaire', $commentaire);
	
	$star = htmlspecialchars($_POST['star']);
	$nom = htmlspecialchars($_POST['nom']);
	$prenom = htmlspecialchars($_POST['prenom']);
	$commentaire = htmlspecialchars($_POST['commentaire']);
	

	if (!(!$nom || !$prenom || !$commentaire  )) {
		$stmt->execute();
		session_start();
		$_SESSION['success_message'] = "Votre avis a été envoyé.";
		header("location:appartements.php#avis");
		exit();
	}
}
